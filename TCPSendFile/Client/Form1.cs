﻿using System;
using System.IO;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using NAL;

namespace Client
{
    public partial class Form1 : Form
    {
        private Socket client = null;
        private int port = 8089;
        private IPEndPoint serverEP = null;

        private TransferHandler transferHandler = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void ConnectServer(string serverIP)
        {
            serverEP = new IPEndPoint(
                IPAddress.Parse(serverIP),
                port);

            client = new Socket(
                AddressFamily.InterNetwork, 
                SocketType.Stream, 
                ProtocolType.Tcp);

            client.BeginConnect(serverEP, new AsyncCallback(ConnectCallback), null);
        }

        private void ConnectCallback(IAsyncResult iar)
        {
            try
            {
                client.EndConnect(iar);
                PrintStatus("已连接至服务器。");
                transferHandler = new TransferHandler(client);
                transferHandler.ReceivedBitmap += new ReceivedBitmapHandler(transferHandler_ReceivedBitmap);
                transferHandler.BeginReceive();
            }
            catch(Exception err)
            {
                PrintStatus(err.Message);
                return;
            }
        }

        void transferHandler_ReceivedBitmap(Bitmap bitmap)
        {
            InsertImage("Server", bitmap);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Title = "选择图片";
            openFile.Filter = "图像文件(*.bmp;*.gif;*.jpg;*.jpeg;*.png)|*.bmp;*.gif;*.jpg;*.jpeg;*.png";
            openFile.Multiselect = false;

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                textBox2.Text = openFile.FileName;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox2.Text.Trim() == "")
                return;

            if (transferHandler != null)
            {
                transferHandler.SendFile(textBox2.Text.Trim());
                InsertImage("Client", new Bitmap(textBox2.Text.Trim()));
            }
            else
                PrintStatus("未建立连接，发送文件失败！");
        }

        private void InsertImage(string name, Bitmap bmp)
        {
            BeginInvoke((MethodInvoker)delegate()
            {
                richTextBox1.AppendText(string.Format("{0} {1}：\r\n", name, DateTime.Now));
                bool isread = richTextBox1.ReadOnly;
                Clipboard.SetDataObject(bmp, false);
                richTextBox1.ReadOnly = false;
                if (richTextBox1.CanPaste(DataFormats.GetFormat(DataFormats.Bitmap)))
                {
                    richTextBox1.Paste();
                    richTextBox1.AppendText("\r\n");
                    richTextBox1.ScrollToCaret();
                }
                richTextBox1.ReadOnly = isread;
            });
        }

        private void PrintStatus(string msg)
        {
            BeginInvoke((MethodInvoker)delegate()
            {
                richTextBox1.AppendText(
                    string.Format("{0}：{1}\r\n",
                    DateTime.Now, msg));
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "")
            {
                textBox1.Select();
                return;
            }

            var exp = @"^([1-9]|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])$";

            Match m = Regex.Match(textBox1.Text.Trim(), exp);
            if (!m.Success)
            {
                MessageBox.Show("IP地址格式错误！IP地址的格式为xxx.xxx.xxx.xxx(xxx为0-255)。",
                    "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                textBox1.SelectAll();
                textBox1.Select();
                return;
            }

            ConnectServer(textBox1.Text.Trim());
        }
    }
}
