﻿using System;
using System.IO;
using System.Drawing;
using System.Net.Sockets;

namespace NAL
{
    public delegate void ReceivedBitmapHandler(Bitmap bitmap);

    public class TransferHandler
    {
        private Socket _client = null;
        public event ReceivedBitmapHandler ReceivedBitmap;

        public TransferHandler(Socket client)
        {
            _client = client;
        }

        public void BeginReceive()
        {
            byte[] buffer = new byte[8];
            //由于long占8位字节，所以先获取前8位字节数据
            IAsyncResult iar = _client.BeginReceive(
                buffer,
                0,
                buffer.Length,
                SocketFlags.None,
                null,
                null);
            int len = _client.EndReceive(iar);
            int offset = 0;
            int length = BitConverter.ToInt32(buffer, offset);  //先获取文件长度
            ReceiveFile(length);

            BeginReceive();  //继续接收
        }

        public void ReceiveFile(long filelen)
        {
            MemoryStream ms = new MemoryStream();
            int bytesRead = 0;
            long count = 0;
            byte[] buffer = new byte[8192];

            while (count != filelen)
            {
                bytesRead = _client.Receive(buffer, buffer.Length, 0);
                ms.Write(buffer, 0, bytesRead);
                count += bytesRead;
            }

            ReceivedBitmap(new Bitmap(ms));
        }

        public void SendFile(string filename)
        {
            FileInfo fi = new FileInfo(filename);

            byte[] len = BitConverter.GetBytes(fi.Length);

            //首先把文件长度发送过去
            _client.BeginSendFile(filename,
                len,
                null,
                TransmitFileOptions.UseDefaultWorkerThread,
                new AsyncCallback(SendFileCallback),
                null);
        }


        private void SendFileCallback(IAsyncResult iar)
        {
            _client.EndSendFile(iar);
        }
    }
}
