﻿using System;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Drawing;
using System.Windows.Forms;
using NAL;

namespace Server
{
    public partial class Form1 : Form
    {
        private Socket listener = null;
        private Socket remoteClient = null;
        private int port = 8089;
        private IPEndPoint listenEP = null;

        private FileStream fs = null;
        private TransferHandler transferHandler = null;

        public Form1()
        {
            InitializeComponent();
            listenEP = new IPEndPoint(
                Dns.GetHostByName(Dns.GetHostName()).AddressList[0].Address, 
                port);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            StartListen();
        }

        private void StartListen()
        {
            listener = new Socket(
                AddressFamily.InterNetwork,                
                SocketType.Stream,                
                ProtocolType.Tcp);
            listener.Bind(listenEP);
            listener.Listen(-1);
            listener.BeginAccept(
                new AsyncCallback(AcceptCallback),
                null);

            PrintStatus(string.Format("监听已开启{0}", listener.LocalEndPoint.ToString()));
        }

        private void AcceptCallback(IAsyncResult iar)
        {
            try
            {
                remoteClient = listener.EndAccept(iar);
                PrintStatus(string.Format("{0}已连接至本地", remoteClient.RemoteEndPoint.ToString()));
                transferHandler = new TransferHandler(remoteClient);
                transferHandler.ReceivedBitmap += new ReceivedBitmapHandler(transferHandler_ReceivedBitmap);
                transferHandler.BeginReceive();
            }
            catch(Exception err)
            {
                PrintStatus(err.Message);
                return;
            }
        }

        void transferHandler_ReceivedBitmap(Bitmap bitmap)
        {
            InsertImage("Client", bitmap);
        }

        private void InsertImage(string name, Bitmap bmp)
        {
            BeginInvoke((MethodInvoker)delegate()
            {
                richTextBox1.AppendText(string.Format("{0} {1}：\r\n", name, DateTime.Now));
                bool isread = richTextBox1.ReadOnly;
                Clipboard.SetDataObject(bmp, false);
                richTextBox1.ReadOnly = false;
                if (richTextBox1.CanPaste(DataFormats.GetFormat(DataFormats.Bitmap)))
                {
                    richTextBox1.Paste();
                    richTextBox1.AppendText("\r\n");
                    richTextBox1.ScrollToCaret();
                }
                richTextBox1.ReadOnly = isread;
            });
        }

        private void PrintStatus(string msg)
        {
            BeginInvoke((MethodInvoker)delegate()
            {
                richTextBox1.AppendText(
                    string.Format("{0}：{1}\r\n",
                    DateTime.Now, msg));
            });
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Title = "选择图片";
            openFile.Filter = "图像文件(*.bmp;*.gif;*.jpg;*.jpeg;*.png)|*.bmp;*.gif;*.jpg;*.jpeg;*.png";
            openFile.Multiselect = false;

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFile.FileName;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "")
                return;

            if (transferHandler != null)
            {
                transferHandler.SendFile(textBox1.Text.Trim());
                InsertImage("Server", new Bitmap(textBox1.Text.Trim()));
            }
            else
                PrintStatus("未建立连接，发送文件失败！");
        }
    }
}
